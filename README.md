# ansible-python

Sample project snippet to call Ansible modules from Python Django
Takes a POST REST call, analyses its body, derives the role which must be run, runs playbook with the roles. 
Takes action on start of task, pass, fail,  fail  with ignored  errors etc.