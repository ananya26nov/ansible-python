import glob
import os

import yaml
from ansible.plugins.callback import CallbackBase

from . import constants


def get_role_tasks(artifact_name, artifact_action):
    role_path = get_role_path(artifact_name, artifact_action)
    task_file_in_role = glob.glob(role_path + "/tasks/main.yml")
    if len(task_file_in_role) == 1:
        task_file_in_role = task_file_in_role[0]
    elif len(task_file_in_role) > 1:
        raise Exception("Invalid ansible role for " + "/".join(artifact_name.split('/')[:-1]) + "_" + artifact_action)
    else:
        raise Exception("Missing ansible role for " + "/".join(artifact_name.split('/')[:-1]) + "_" + artifact_action)

    sub_task_names = []
    with open(task_file_in_role, 'r') as infile:
        try:
            data = yaml.safe_load(infile)
        except Exception as exp:
            raise Exception("Invalid yaml format in ansible role for " + "/".join(artifact_name.split('/')[:-1]) + "_" + artifact_action + " : " + str(exp))
        if not data:
            raise Exception("No tasks found in ansible role for " + "/".join(artifact_name.split('/')[:-1]) + "_" + artifact_action)
        try:
            for task in data:
                if len(task['name']) > 0:
                    sub_task_names.append(task['name'])
                else:
                    error_msg = "Task name missing in ansible role : " + \
                                    "/".join(artifact_name.split('/')[:-1])
                    raise Exception(error_msg)
            return sub_task_names
        except yaml.YAMLError as exc:
            raise exc
        except KeyError as exc:
            raise KeyError("Ansible role for " + "/".join(artifact_name.split('/')[:-1]) + " is missing task name")



def get_role_base_path():
    return get_env_value("ROLE_BASE_PATH")


def get_role_path(artifact_name, artifact_action):
    role_path_folder_base = [i.lower() for i in artifact_name.split("/")[:-1]]
    role_path = "_".join([
        os.path.join(get_role_base_path(), os.path.join(*role_path_folder_base)),
        artifact_action])
    return role_path


def get_roles(item):
    roles = []
    name = item["name"]
    action = item["action"]
    role_path = get_role_path(name, action)
    roles.append(role_path)
    return roles




def get_site_properties_for_ansible():
    """
    Gets the extra-vars string for user name and password of the router
    :return: string of username and password
    """
    pass


def extract_playbook_ouput_lines(playbook_results):
    """
    Extracts the output lines from playbook output
    :param playbook_output: output from playbook execution
    :return:
    """
    host_ok = playbook_results['host_ok']
    host_failed = playbook_results['host_failed']
    host_unreachable = playbook_results['host_unreachable']


def get_dest_file_name(router):
    '''
    :param router:
    :return:
    '''
    return router.name + '_' + router.type + '.txt'


def get_config_file_path(config_file):
    '''
    Gets the config file path from router properties
    :param router: router object
    :return: config file path
    '''
    return os.path.join(constants.CONFIG_FILES_DIRECTORY, config_file)


def get_extra_vars(router_detail, config_file_path=""):
    '''
    Gets the extra-vars string for playbook
    :param router_detail: router details object
    :param config_file_path: config file path
    :return: extra vars dict
    '''
    extra_vars_dict = {}
    extra_vars_dict['host_name'] = router_detail.router.ip_address
    extra_vars_dict['config_file'] = config_file_path
    extra_vars_dict['destination_file'] = get_config_file_path(
        get_dest_file_name(router_detail.router))

    # Add variables for R2 router
    if type(router_detail) is models.R2L3VPNDetailsForm:
        extra_vars_dict['import_rt'] = router_detail.import_rt
        extra_vars_dict['export_rt'] = router_detail.export_rt
        extra_vars_dict['rd'] = router_detail.rd
        extra_vars_dict['interface'] = router_detail.interface
        extra_vars_dict['vlan'] = router_detail.vlan

    # Variable for both R2 and DCR
    extra_vars_dict['vrf_name'] = router_detail.vrf_name
    extra_vars_dict['router_name'] = router_detail.router_name

    extra_vars_dict.update(get_site_properties_for_ansible())

    return extra_vars_dict


def get_playbook_command(playbook_path, extra_vars):
    '''
    Gets the command for the playbook
    :param playbook_path: playbook path
    :param extra_vars: extra-vars string
    :return: command to execute playbook
    '''
    return 'ansible-playbook ' + playbook_path + ' --extra-vars "' + extra_vars + '"'


# def execute_playbook(playbook_file, extra_vars_dict, tags=[]):
#     '''
#     Executes the playbook with passed in extra-vars
#     :param playbook_file: playbook file path
#     :param extra_vars_dict: extra-vars for playbook
#     :param tags: tags for playbook
#     :return:
#     '''
#     results_callback = callback_loader.get('json')
#     loader = DataLoader()
#     inventory = InventoryManager(loader=loader, sources=[constants.HOSTS_FILE])
#     variable_manager = VariableManager(loader=loader, inventory=inventory)
#     variable_manager.extra_vars = extra_vars_dict
#
#     passwords = {}
#
#     Options = namedtuple('Options',
#                          ['connection', 'remote_user', 'ask_sudo_pass',
#                           'verbosity', 'ack_pass',
#                           'module_path', 'forks', 'become', 'become_method',
#                           'become_user', 'check', 'listhosts',
#                           'listtasks', 'listtags', 'syntax', 'sudo_user',
#                           'sudo', 'diff', 'tags'])
#
#     options = Options(connection='local', remote_user=None, ack_pass=None,
#                       sudo_user=None, forks=5, sudo=None,
#                       ask_sudo_pass=False, verbosity=5, module_path=None,
#                       become=None, become_method=None,
#                       become_user=None, check=False, diff=False, listhosts=None,
#                       listtasks=None, listtags=None,
#                       syntax=None, tags=tags)
#
#     playbook = PlaybookExecutor(playbooks=[playbook_file], inventory=inventory,
#                                 variable_manager=variable_manager,
#                                 loader=loader, options=options,
#                                 passwords=passwords)
#     playbook._tqm._stdout_callback = ResultsCollector()
#     output_result = playbook.run()
#     output = {}
#     output['status'] = output_result
#     output['ok'] = playbook._tqm._stdout_callback.host_ok
#     output['unreachable'] = playbook._tqm._stdout_callback.host_unreachable
#     output['failed'] = playbook._tqm._stdout_callback.host_failed
#     extracted_output = extract_playbook_ouput_lines(output)
#     print(output)
#     print(extracted_output)
#     return extracted_output


class ResultsCollector(CallbackBase):
    '''Callback class for ansible playbook execution'''

    def __init__(self, *args, **kwargs):
        super(ResultsCollector, self).__init__(*args, **kwargs)
        self.host_ok = []
        self.host_unreachable = []
        self.host_failed = []

    def v2_runner_on_unreachable(self, result, ignore_errors=False):
        name = result._host.get_name()
        task = result._task.get_name()
        self.host_unreachable.append(
            dict(ip=name, task=task, output_lines=result._result))

    def v2_runner_on_ok(self, result, *args, **kwargs):
        name = result._host.get_name()
        task = result._task.get_name()
        if task == "setup":
            pass
        elif "Info" in task:
            self.host_ok.append(
                dict(ip=name, task=task, output_lines=result._result))
        else:
            self.host_ok.append(
                dict(ip=name, task=task, output_lines=result._result))

    def v2_runner_on_failed(self, result, *args, **kwargs):
        name = result._host.get_name()
        task = result._task.get_name()
        self.host_failed.append(
            dict(ip=name, task=task, output_lines=result._result))


class AnsibleResult():
    "Holds the results of the ansible output"

    def __init__(self):
        self.is_passed = False
        self.router_name = ''
        self.status_ok = []
        self.status_failed = []
        self.status_unreachable = []

    def __str__(self):
        output_str = 'STATUS - ' + (
            'PASSED' if self.is_passed else 'FAILED') + '\n\n'
        for status in self.status_ok:
            output_str += status.task + ' - PASSED\n'
            output_str += '\t' + status.std_out_ok + '\n'
            output_str += '\n'

        for status in self.status_failed:
            output_str += status.task + ' - FAILED\n'
            output_str += '\tMESSAGE - ' + status.msg + '\n'
            output_str += '\n'

        for status in self.status_unreachable:
            output_str += status.task + ' - FAILED\n'
            output_str += '\tMESSAGE - ' + status.msg + '\n'

        return output_str


class AnsibleStatus(object):
    "Ansible status for each task"

    def __init__(self):
        self.task = ''
        self.msg = ''
        self.msg_std_err = ''
        self.std_out_ok = ''

