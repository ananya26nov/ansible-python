"""List Artifact Controller to get the Arifacts Lists from NEXUS"""

import threading

from django.utils.datastructures import MultiValueDictKeyError
from rest_framework.response import Response
from rest_framework.views import APIView

from . import constants
from . import tasks


class AnsiblePythonController(APIView):
    """To list artifacts of a type"""

    def __init__(self):
        super(AnsiblePythonController, self).__init__()

    def post(self, request):
        """function to unlock lock_name"""
        try:
            try:
                _some_key = request.data[constants.SOME_KEY]
            except (MultiValueDictKeyError, KeyError) as arg:
                raise ValueError(arg)
            thread = WorkflowThread(request)
            thread.start()
            return Response(
                {"msg": "Process Started"})
        except ValueError as exp:
            return Response(
                {"title": "Error in request values", "detail": str(exp)})
        except Exception as exp:
            return Response({"title": "Unknown Error",
                                        "detail": str(exp)})


class WorkflowThread(threading.Thread):
    """Thread Implementation"""

    def __init__(self, request):
        threading.Thread.__init__(self)
        self.request = request

    def run(self):
        try:
            tasks.ansible_workflow(self.request)
        except Exception as exp:
            raise exp
