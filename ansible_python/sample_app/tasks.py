import os
import shutil
from datetime import datetime

import ansible.constants as C
from ansible import context
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.inventory.manager import InventoryManager
from ansible.module_utils.common.collections import ImmutableDict
from ansible.parsing.dataloader import DataLoader
from ansible.playbook.play import Play
from ansible.plugins.callback import CallbackBase
from ansible.vars.manager import VariableManager
from django.conf import settings

from .ansible_tasks import get_roles


class ResultCallback(CallbackBase):
    """A sample callback plugin used for performing an action as results come in

    If you want to collect all results into a single object for processing at
    the end of the execution, look into utilizing the ``json`` callback plugin
    or writing your own custom callback plugin
    """
    def __init__(self, *args, **kwargs):
        super(ResultCallback, self).__init__(*args, **kwargs)
        self.host_ok = []
        self.host_unreachable = []
        self.host_failed = []
        self.start_time = datetime.now()
        self.workflow_id = ""
        self.parent_task_name = ""

    def set_workflow_id(self, workflow_id):
        self.workflow_id = workflow_id

    def set_parent_task_name(self, name):
        self.parent_task_name = name

    def v2_playbook_on_task_start(self, task, is_conditional):
        print(
            "Task in progress " + str(task.name) +
            " : " + self.parent_task_name)

    def v2_runner_on_unreachable(self, result, ignore_errors=False):
        name = result._host.get_name()
        task = result._task.name
        self.host_unreachable.append(dict(ip=name, task=task, output_lines=result._result))

    def v2_runner_on_ok(self, result,  *args, **kwargs):
        name = result._host.get_name()
        task = result._task.name
        if task == "setup":
            pass
        elif "Info" in task:
            self.host_ok.append(dict(ip=name, task=task, output_lines=result._result))
        else:
            self.host_ok.append(dict(ip=name, task=task, output_lines=result._result))

    def v2_runner_on_failed(self, result,   *args, **kwargs):
        try:
            name = result._host.get_name()
            task = result._task.name
            error_msg = []
            if 'results' in result._result:
                for msg in result._result['results']:
                    error_msg.append(str(msg['msg']))
                    if 'stderr' in result._result:
                        stderr_error = result._result['stderr']
                        error_msg.append(" : " + stderr_error)
            elif 'msg' in result._result:
                error_msg.append(str(result._result['msg']))
                if 'stderr' in result._result:
                    stderr_error = result._result['stderr']
                    error_msg.append(" : " + stderr_error)
            error_msg = ','.join(error_msg)
            if len(error_msg) > 490:
                error_msg = error_msg[:490]
            if result._task.ignore_errors:
                print(
                    "Error/s : " + error_msg)
            else:
                print(
                    "Error/s : " + error_msg)
            self.host_failed.append(dict(ip=name, task=task, output_lines=result._result))
        except Exception as exp:
            raise exp


def get_artifact_paths(folder):
    # List all artifacts from the repo
    try:
        ip =  get_env_value('NEXUS_SERVICE_HOST')
        port = get_env_value('NEXUS_SERVICE_PORT')
        ip = ip + ':' + port
        reponame = get_env_value('REPONAME')
        url = nexus.form_url(ip=ip, action='artifact_list',  reponame=reponame, proto='http')
        uname = get_env_value('NEXUS_USER')
        passwd = get_env_value('NEXUS_PASS')
        artifacts_list_response = nexus.list_all_artifacts(url, uname, passwd)
        # Read response and find only paths of the artifacts
        artifacts_list = nexus.list_artifact_download_paths(artifacts_list_response)
        artifacts_path_list = []
        # Filter artifacts for our folder
        for path in artifacts_list:
            if path.startswith(folder['name']):
                artifacts_path_list.append(path)
        if len(artifacts_path_list) < 1:
            raise Exception("No artifacts are present on folder " + folder['name'])
        return artifacts_path_list
    except Exception as exp:
        raise exp


def get_inventory_name():
    return os.path.join(settings.MEDIA_ROOT, "hosts")


def create_inventory():
    deployer_ansible_host = get_env_value('DEPLOYER_ANSIBLE_HOST')
    deployer_ansible_user = get_env_value('DEPLOYER_ANSIBLE_USER')
    deployer_ansible_ssh_private_key = get_env_value('DEPLOYER_ANSIBLE_SSH_PRIVATE_KEY')
    inventory_str = "[deploy_host]\ndeployer ansible_host=" + \
                    deployer_ansible_host + " ansible_user=" + \
                    deployer_ansible_user + " ansible_ssh_private_key_file=" +\
                    deployer_ansible_ssh_private_key + \
                    " ansible_ssh_common_args='-o StrictHostKeyChecking=no'"

    if not os.path.exists(settings.MEDIA_ROOT):
        os.makedirs(settings.MEDIA_ROOT)
    outfile = open(get_inventory_name(), "w")
    outfile.write(inventory_str)
    outfile.close()


def delete_inventory():
    remove_file(get_inventory_name())


def ansible_workflow(workflow_id, request):
    data = request.data
    context.CLIARGS = ImmutableDict(connection='ssh', module_path=None, forks=10, become=None,
                                    become_method=None, become_user=None, check=False, diff=False, verbosity=5)

    loader = DataLoader()
    passwords = dict(vault_pass='secret')
    results_callback = ResultCallback()
    results_callback.set_workflow_id(workflow_id)
    create_inventory()
    inventory = InventoryManager(loader=loader, sources=os.path.join(settings.MEDIA_ROOT, "hosts"))
    variable_manager = VariableManager(loader=loader, inventory=inventory)

    for item in data["subgroups"]:
        name = item["name"]
        action = item["action"]
        task_name = action + " " + name.split('/')[-1]
        results_callback.set_parent_task_name(task_name)
        try:
            roles = get_roles(item)
            artifact_paths = get_artifact_paths(item)
            downloadable_paths = []
            for path in  artifact_paths:
                ip = get_env_value('NEXUS_SERVICE_HOST')
                port = get_env_value('NEXUS_SERVICE_PORT')
                ip = ip + ':' + port
                uname = get_env_value('NEXUS_USER')
                passwd = get_env_value('NEXUS_PASS')
                reponame = get_env_value('MIRROR_REPONAME')
                downloadable_path = nexus.form_url(ip=ip, action='artifact_download',
                                                   reponame=reponame, proto='http',
                                                   uname=uname,
                                                   passwd=passwd) + path
                downloadable_paths.append(
                    {
                        "name":downloadable_path.split("/")[-1],
                        "path":downloadable_path
                    }
                )
            vars_list = [
                {'artifacts_to_download': downloadable_paths},
                {'download_directory_path': '_'.join([i.lower() for i in
                                     item['name'].split("/")[:]])}
            ]
            play_source =  dict(
                    name = "Ansible Play",
                    hosts = 'deploy_host',
                    gather_facts = 'no',
                    vars = vars_list,
                    roles = roles
                )

            play = Play().load(play_source, variable_manager=variable_manager, loader=loader)

            tqm = None
            try:
                tqm = TaskQueueManager(
                          inventory=inventory,
                          variable_manager=variable_manager,
                          loader=loader,
                          passwords=passwords,
                          stdout_callback=results_callback,
                      )
                result = tqm.run(play)
            finally:
                if tqm is not None:
                    tqm.cleanup()

                # Remove ansible tmpdir
                shutil.rmtree(C.DEFAULT_LOCAL_TMP, True)

        except Exception as exp:
            raise exp

    delete_inventory()
    print("DONE")

